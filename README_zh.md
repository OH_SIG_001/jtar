# tar

## 简介
> tar支持tar打包和tar解包功能。

## 效果展示
![avatar](screenshot/app.jpeg)![avatar](screenshot/cmd.jpg)

## 下载安装
```shell
ohpm install @ohos/tar
```
OpenHarmony ohpm环境配置等更多内容，请参考 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。

## 使用说明

1. 在page页面引入包

 ```
import { OHOSTar, GlobalContext } from "@ohos/tar"
import fs from '@ohos.file.fs';
 ```
2. 创建OHOStarTestInit类

 ```
 class OHOStarTestInit {

  private static instance: OHOStarTestInit;
  rootPath: string = GlobalContext.getContext().getObject('filesDir') as string

  public static getInstance(): OHOStarTestInit {
    if (!OHOStarTestInit.instance) {
      OHOStarTestInit.instance = new OHOStarTestInit();
    }
    return OHOStarTestInit.instance;
  }

  public GetDirName(): string {
    return this.rootPath + '/' + "tartestdir";
  }

  // tartest初始化
  TestTarInit(): void {
    fs.mkdir(this.GetDirName(), (err) => {
      if (err) {
        console.info("mkdir failed with error message:" + err.message + ",errorcode:" + err.code);
      } else {
        let filePath = this.GetDirName() + "/test.txt";
        let file = fs.openSync(filePath, fs.OpenMode.READ_WRITE | fs.OpenMode.CREATE);
        fs.write(file.fd, "1234567890abcdefghij", (err) => {
          if (err) {
            console.info("write failed with error message: " + err.message + " error code: " + err.code);
          } else {
            fs.closeSync(file);
          }
        });
      }
    });
  }

  // untartest初始化
  TestUnTarInit(): void {
    fs.rmdir(this.GetDirName()); // 删除 tartestdir目录
  }

  //重置
  public DirReset() {
    fs.listFile(this.rootPath, (err, filenames) => {
      if (err) {
        console.info("list file failed with error message: " + err.message + ", error code: " + err.code);
      } else {
        for (let i = 0; i < filenames.length; i++) {
          let deleteFile = this.rootPath + '/' + filenames[i]
          if(fs.statSync(deleteFile).isDirectory()){
            fs.rmdirSync(deleteFile);
          }else{
            fs.unlink(deleteFile);
          }
        }
      }
    });
  }
}
 ```
3. 创建tar工具对象
 ```
  // 参数1：要操作的文件或文件夹的父目录；
  // 参数2：压缩生成的文件名；
  let tar: OHOSTar = new OHOSTar(OHOStarTestInit.getInstance().rootPath, "testdir");
 ```
4. 初始化Tar测试环境，创建生成tartestdir目录，并且包含一些txt文件

 ```
   HOStarTestInit.getInstance().TestTarInit();
 ```
5. Tar功能测试，运行后会将环境中的tartestdir目录打包为tartest.tar文件

 ```
   // 要压缩的文件夹名称
   tar.addTarPath("tartestdir");
   tar.tar();
 ```
6. 初始化Untar测试环境

 ```
   OHOStarTestInit.getInstance().TestUnTarInit();
 ```
7. Untar功能测试，运行后会将环境中的tartest.tar文件解包

 ```
   // 参数 path： 自定义解压路径，例：folder1/folder2，这个path是从压缩操作的父目录后面开始的目录（即rootPath后面）
   tar.untar("customUnTarPath/888");
 ```

## 以上功能测试均可通过在设备应用目录中查看

在设备shell中:

1. 进入到应用files目录中

```
   # cd /data/app/el2/100/base/cn.openharmony.jtar/haps/entry/files
```
2. 点击Tar Init，会生成tartestdir，tartestdir中包含一个txt文件

```
   # ls
   tartestdir
   # ls tartestdir/
   test.txt
```
3. 点击Tar，会打包生成对应的tartest.tar文件

```
   # ls
   tartest.tar  tartestdir
```
4. 点击UnTar Init，生成对应的tartest.tar文件

```
   # ls
   tartest.tar
```
5. 点击UnTar，生成对应的tartest.tar文件的原始文件

```
   # ls
   tartest.tar  tartestdir
   # ls tartestdir
   test.txt
```

## 接口说明

1. 修改生成的tar文件名
   `setTarName(name: string)`
   
2. 修改解包目标的Untar文件名
   `setUnTarName(name: string)`
   
3. 修改解包目标的Untar路径
   `setUnTarPath(path: string)`

4. 添加一个需要tar的文件或目录
   `addTarPath(path: string)`

5. 删除一个需要tar的文件或目录
   `delTarPath(path: string)`

6. tar打包
   `tar(): number`

8. untar解包
   `untar(path?: string): number`


## 源码下载
本项目依赖tar库，通过`git submodule`引入，下载代码时需加上`--recursive`参数。
  ```
  git clone --recursive https://gitee.com/openharmony-sig/jtar.git
  ```

## 约束与限制

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release(5.0.0.66)

## 目录结构
````
|---- jtar  
|     |---- entry  # 示例代码文件夹
|     |---- library  # tar库文件夹
|        |---- src 
|           |---- main 
|                 |---- cpp  # c++详细接口
|                 |---- ets
|                       |---- ohos_tar.ets  # 接口封装
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法                    
|     |---- README_zh.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/jtar/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/jtar/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/jtar/blob/master/LICENSE) ，请自由地享受和参与开源。