# tar

## Introduction
**tar** provides APIs for packaging files and directories into a single archive file and extracting the contents from an archive file.

## Effect
![avatar](screenshot/app.jpeg)![avatar](screenshot/cmd.jpg)

## How to Install
```shell
ohpm install @ohos/tar
```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

1. Import modules.

 ```
import { OHOSTar, GlobalContext } from "@ohos/tar"
import fs from '@ohos.file.fs';
 ```
2. Create the **OHOStarTestInit** class.

 ```
 class OHOStarTestInit {

  private static instance: OHOStarTestInit;
  rootPath: string = GlobalContext.getContext().getObject('filesDir') as string

  public static getInstance(): OHOStarTestInit {
    if (!OHOStarTestInit.instance) {
      OHOStarTestInit.instance = new OHOStarTestInit();
    }
    return OHOStarTestInit.instance;
  }

  public GetDirName(): string {
    return this.rootPath + '/' + "tartestdir";
  }

  // Initialize tartest.
  TestTarInit(): void {
    fs.mkdir(this.GetDirName(), (err) => {
      if (err) {
        console.info("mkdir failed with error message:" + err.message + ",errorcode:" + err.code);
      } else {
        let filePath = this.GetDirName() + "/test.txt";
        let file = fs.openSync(filePath, fs.OpenMode.READ_WRITE | fs.OpenMode.CREATE);
        fs.write(file.fd, "1234567890abcdefghij", (err) => {
          if (err) {
            console.info("write failed with error message: " + err.message + " error code: " + err.code);
          } else {
            fs.closeSync(file);
          }
        });
      }
    });
  }

  // Initialize untartest.
  TestUnTarInit(): void {
    fs.rmdir(this.GetDirName()); // Delete the tartestdir directory.
  }

  // Reset
  public DirReset() {
    fs.listFile(this.rootPath, (err, filenames) => {
      if (err) {
        console.info("list file failed with error message: " + err.message + ", error code: " + err.code);
      } else {
        for (let i = 0; i < filenames.length; i++) {
          let deleteFile = this.rootPath + '/' + filenames[i]
          if(fs.statSync(deleteFile).isDirectory()){
            fs.rmdirSync(deleteFile);
          }else{
            fs.unlink(deleteFile);
          }
        }
      }
    });
  }
}
 ```
3. Create a tar instance.
 ```
  // Parameter 1: parent directory of the file or folder to operate.
  // Parameter 2: name of the compressed file.
  let tar: OHOSTar = new OHOSTar(OHOStarTestInit.getInstance().rootPath, "testdir");
 ```
4. Initialize the test environment for packaging files, and create the **tartestdir** directory with a .txt file in it.

 ```
   HOStarTestInit.getInstance().TestTarInit();
 ```
5. Package the **tartestdir** directory as **tartest.tar**.

 ```
   // Name of the folder to package
   tar.addTarPath("tartestdir");
   tar.tar();
 ```
6. Initialize the test environment for extracting files from a tar archive. 

 ```
   OHOStarTestInit.getInstance().TestUnTarInit();
 ```
7. Extract files from **tartest.tar**.

 ```
   // The parameter path specifies the directory in which the files are extracted, for example, folder1/folder2. The path is a directory under the parent directory (rootPath) of the operation.
   tar.untar("customUnTarPath/888");
 ```

## Verification

On the shell of the device, check the tested functions in the application directory.

1. Go to the **files** directory of the application.

```
   # cd /data/app/el2/100/base/cn.openharmony.jtar/haps/entry/files
```
2. Check the **tartestdir** directory and the .txt file generated after **Tar Init** is clicked.

```
   # ls
   tartestdir
   # ls tartestdir/
   test.txt
```
3. Check the **tartest.tar** file that is generated after **Tar** is clicked.

```
   # ls
   tartest.tar  tartestdir
```
4. Check the **tartest.tar** file that is generated after **UnTar Init** is clicked.

```
   # ls
   tartest.tar
```
5. Check the files that are extracted from **tartest.tar** after **UnTar** is clicked.

```
   # ls
   tartest.tar  tartestdir
   # ls tartestdir
   test.txt
```

## Available APIs

| API                            | Description                 |
| ------------------------------ | --------------------------- |
| `setTarName(name: string)`     | Sets the name of the generated .tar file.        |
| `setUnTarName(name: string)`   | Sets the name of the .tar file to be extracted.  |
| `setUnTarPath(path: string)`   | Sets the directory where the .tar file is extracted.    |
| `addTarPath(path: string)`     | Adds a file or directory to be included in a tar archive.|
| `delTarPath(path: string)`     | Deletes a file or directory.|
| `tar(): number`                | Creates a tar archive.                    |
| `untar(path?: string): number` | Extracts the contents of a tart archive.                  |




## Downloading Source Code
This project depends on the tar library. You can use **git** *submodule* with the **--recursive** parameter to download the source code.
  ```
  git clone --recursive https://gitee.com/openharmony-sig/jtar.git
  ```

## Constraints

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)

## Project Directory
````
|---- jtar  
|     |---- entry  # Sample code
|     |---- library  # tar library
|        |---- src 
|           |---- main 
|                 |---- cpp  # c++ APIs
|                 |---- ets
|                       |---- ohos_tar.ets  # API encapsulation
|           |---- index.ets  # External APIs
|     |---- README.md  # Readme                   
|     |---- README_zh.md  # Readme                   
````

## How to Contribute
If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-sig/jtar/issues) or a [PR](https://gitee.com/openharmony-sig/jtar/pulls).

## License
This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/jtar/blob/master/LICENSE).
